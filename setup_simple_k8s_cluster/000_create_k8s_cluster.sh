#!/bin/bash

vagrant destroy -f

./01_k8s_init_vm.sh
./02_k8s_join_nodes.sh
./03_k8s_make_context.sh
#./04_local_net.bridge.bridge_enabler.sh
./05_create_admin_user.sh