#!/bin/bash

set -eu

vagrant ssh kub2 < tmp/add_node_to_k8s.sh
vagrant ssh kub3 < tmp/add_node_to_k8s.sh

#vagrant snapshot save clean_vms
