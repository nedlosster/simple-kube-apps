cat << EOF | kubectl create -n kubernetes-dashboard -f -
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard-nodeport
  namespace: kubernetes-dashboard
spec:
  type: NodePort
  ports:
    - port: 443
      targetPort: 8443
      nodePort: 30123
  selector:
    k8s-app: kubernetes-dashboard
EOF
