#!/bin/bash

set -eu

mkdir -p tmp

vagrant up

vagrant ssh minikube -c '/vagrant/setup_minikube.sh'
vagrant ssh minikube -c 'minikube start'
vagrant ssh minikube -c 'minikube addons enable dashboard'
vagrant ssh minikube -c 'minikube addons enable registry'
