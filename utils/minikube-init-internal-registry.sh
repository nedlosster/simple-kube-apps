#!/bin/bash

set -eu

echo "1. Enable minikube registry addon"

minikube addons enable registry

echo "2. Run custom werf-registry service with the binding to 5000 port"

kubectl -n kube-system expose rc/registry --type=ClusterIP --port=5000 --target-port=5000 --name=werf-registry --selector='actual-registry=true'

echo "3. Setup registry domain in minikube VM"

export REGISTRY_IP=$(kubectl -n kube-system get svc/werf-registry -o=template={{.spec.clusterIP}})
minikube ssh "echo '$REGISTRY_IP werf-registry.kube-system.svc.cluster.local' | sudo tee -a /etc/hosts"

echo "4. Setup registry domain in host system"

echo "127.0.0.1 werf-registry.kube-system.svc.cluster.local" | sudo tee -a /etc/hosts


echo "5. Run port forwarder on host system in a separate terminal"

kubectl port-forward --namespace kube-system service/werf-registry 5000

echo "6. Check connectivity on host system"

curl -X GET werf-registry.kube-system.svc.cluster.local:5000/v2/_catalog