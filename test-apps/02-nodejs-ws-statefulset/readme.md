# Демонстрация проблемы с vscode.

Тут пушиться в миникуб образ двумя путями - через реестр контейнеров и напрямую в миникуб.

в итоге получаются два пода:

```
nedlosster@nedlosster-ws:~/projects/simple-kube-apps/test-apps/02-nodejs-ws-statefulset$ k describe pod nodejs-ws-0 -n mcn

Name:         nodejs-ws-0
Namespace:    mcn
Priority:     0
Node:         minikube/172.17.0.3
Start Time:   Fri, 29 May 2020 14:30:31 +0300
Labels:       app=nodejs-ws
              controller-revision-hash=nodejs-ws-5b5584949d
              statefulset.kubernetes.io/pod-name=nodejs-ws-0
Annotations:  <none>
Status:       Running
IP:           172.18.0.8
IPs:
  IP:           172.18.0.8
Controlled By:  StatefulSet/nodejs-ws
Containers:
  nodejs-ws:
    Container ID:  docker://e6d7fc898ce2b9e3e22a86c40a3ae0be29f1cf5d0ec275f711a0366b70b8e1f4
    Image:         mcn/nodejs-ws:1.5
    Image ID:      docker://sha256:85ce479e19e55c35ef0c8524583cf8c6de5f4dbe09daa6de9ba29e85b013abbf
    Ports:         8080/TCP, 22/TCP
    Host Ports:    0/TCP, 0/TCP
    Command:
      /bin/sh
    Args:
      -c
      while sleep 1000; do :; done
    State:          Running
      Started:      Fri, 29 May 2020 14:30:32 +0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-h8hc8 (ro)
      /workspace from workspace (rw)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  workspace:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  workspace-nodejs-ws-0
    ReadOnly:   false
  default-token-h8hc8:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-h8hc8
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  3m4s  default-scheduler  Successfully assigned mcn/nodejs-ws-0 to minikube
  Normal  Pulled     3m3s  kubelet, minikube  Container image "mcn/nodejs-ws:1.5" already present on machine
  Normal  Created    3m3s  kubelet, minikube  Created container nodejs-ws
  Normal  Started    3m3s  kubelet, minikube  Started container nodejs-ws

```

и

```
nedlosster@nedlosster-ws:~/projects/simple-kube-apps/test-apps/02-nodejs-ws-statefulset$ k describe pod nodejs-ws-reg-0 -n mcn

Name:         nodejs-ws-reg-0
Namespace:    mcn
Priority:     0
Node:         minikube/172.17.0.3
Start Time:   Fri, 29 May 2020 14:30:32 +0300
Labels:       app=nodejs-ws
              controller-revision-hash=nodejs-ws-reg-65586f9bd6
              statefulset.kubernetes.io/pod-name=nodejs-ws-reg-0
Annotations:  <none>
Status:       Running
IP:           172.18.0.9
IPs:
  IP:           172.18.0.9
Controlled By:  StatefulSet/nodejs-ws-reg
Containers:
  nodejs-ws:
    Container ID:  docker://8924556cfd555989fd56bc9f7c91fe8d887eb82fcd283524907895b813b0196a
    Image:         127.0.0.1:5000/nodejs-ws:1.5
    Image ID:      docker-pullable://127.0.0.1:5000/nodejs-ws@sha256:27b462fa3de1551462cbb3ec7ff07716b871d29671c6445b9a3c82bdc435e743
    Ports:         8080/TCP, 22/TCP
    Host Ports:    0/TCP, 0/TCP
    Command:
      /bin/sh
    Args:
      -c
      while sleep 1000; do :; done
    State:          Running
      Started:      Fri, 29 May 2020 14:30:32 +0300
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-h8hc8 (ro)
      /workspace from workspace (rw)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  workspace:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  workspace-nodejs-ws-reg-0
    ReadOnly:   false
  default-token-h8hc8:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-h8hc8
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  4m15s  default-scheduler  Successfully assigned mcn/nodejs-ws-reg-0 to minikube
  Normal  Pulled     4m15s  kubelet, minikube  Container image "127.0.0.1:5000/nodejs-ws:1.5" already present on machine
  Normal  Created    4m15s  kubelet, minikube  Created container nodejs-ws
  Normal  Started    4m15s  kubelet, minikube  Started container nodejs-ws

```

при попытке присоединения к контейнеру спуленного с реестра вскод падает с ошибкой:

```
Unable to open 'workspace': Unable to read file 'vscode-remote://k8s-container+podname=nodejs-ws-reg-0+namespace=mcn+name=nodejs-ws+image=127.0.0.1:5000/nodejs-ws:1.5/workspace' (TypeError: Cannot destructure property 'contents' of 'this._folderConfigurations.get(...)' as it is undefined.).
```

Думаю, проблема в том, что в строке 

1. Вариант:   Image ID:      docker://sha256:85ce479e19e55c35ef0c8524583cf8c6de5f4dbe09daa6de9ba29e85b013abbf
2. Вариант:   Image ID:      docker-pullable://127.0.0.1:5000/nodejs-ws@sha256:27b462fa3de1551462cbb3ec7ff07716b871d29671c6445b9a3c82bdc435e743

vscode спотыкается на ip-адресе.
Скорее всего проблема решиться, если наш приватный реестр контейнеров будет доступен без необходимости указывать номер порта.
